package main

import (
	"fmt"
	"math"
)

func main() {
	var amount, period, rate float64
	var year float64 = 12

	fmt.Print("Write amount of Credit:  ")
	fmt.Scan(&amount)
	fmt.Print("Write a Rate; ")
	fmt.Scan(&rate)
	fmt.Print("Write period in years: ")
	fmt.Scan(&period)

	rate = rate / 100

	Amortization(amount, period, rate, year)
}

func Amortization(amount, period, rate, year float64) {
	var pay, totalPayment, interest, payment, balanse float64

	pay = (1 - (math.Pow(1 + rate/year, - year*period))) / (rate / year)
	totalPayment = amount / pay
	balanse = amount
	month := 1
	
	fmt.Print("Month\tPayment\tInterest\tPrincipal\tBalance")
	
	for month <= int(period)*int(year) {
		interest = balanse * rate / year
		payment = totalPayment - interest
		balanse = balanse - payment

		fmt.Printf("\n| %v\t| %.2f \t| %.2f \t| %.2f \t| %.2f \t   \n", month, totalPayment, payment, interest, balanse)

		month++
	}
}